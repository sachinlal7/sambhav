import 'package:flutter/material.dart';

class MediumTask extends StatelessWidget {
  const MediumTask({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Container(
          width: double.maxFinite,
          height: 95,
          color: const Color.fromARGB(255, 220, 220, 220),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "Medium Priority Task",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Center(
                    child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Text("No Task Available"),
                ))
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
