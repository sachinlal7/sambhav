import 'package:flutter/material.dart';

import 'containerbox.dart';

class Missed extends StatelessWidget {
  const Missed({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Container(
          width: double.maxFinite,
          color: const Color.fromARGB(255, 188, 222, 249),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "Missed Task",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ContainerBox(
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
