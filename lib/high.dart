import 'package:flutter/material.dart';

import 'containerbox.dart';

class HighTask extends StatelessWidget {
  const HighTask({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Container(
          width: double.maxFinite,
          color: const Color.fromARGB(255, 248, 185, 206),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "High Priority Task",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ContainerBox(
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
