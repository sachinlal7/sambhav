import 'package:flutter/material.dart';
import 'package:sambhav/Task/task12.dart';
import 'package:sambhav/task1.dart';

import 'containerbox.dart';

class AllTask extends StatelessWidget {
  const AllTask({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        children: [
          Container(
            width: double.maxFinite,
            color: const Color.fromARGB(255, 245, 233, 197),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // const Padding(
                  //   padding: EdgeInsets.all(5.0),
                  //   child: Text(
                  //     "All Task",
                  //     style: TextStyle(fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) {
                            return const Task11();
                          },
                        ));
                      },
                      child: ContainerBox(
                        color: Colors.orange,
                        type: "all_task",
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) {
                            return const Task12();
                          },
                        ));
                      },
                      child: ContainerBox(
                          color: Colors.orange,
                          type: "all_task",
                          statusDisplay: "Completed"),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              width: double.maxFinite,
              color: const Color.fromARGB(255, 188, 222, 249),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(5.0),
                      // child: Text(
                      //   "Missed Task",
                      //   style: TextStyle(fontWeight: FontWeight.bold),
                      // ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 8),
                      child: ContainerBox(
                        type: "missed",
                        color: const Color.fromARGB(255, 73, 163, 237),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              width: double.maxFinite,
              color: const Color.fromARGB(255, 248, 185, 206),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(5.0),
                      // child: Text(
                      //   "High Priority Task",
                      //   style: TextStyle(fontWeight: FontWeight.bold),
                      // ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      child: ContainerBox(
                        color: Colors.pink,

                        // iconData1: Icons.link_sharp,
                        // iconData2: Icons.location_on_outlined,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              width: double.maxFinite,
              height: 95,
              color: const Color.fromARGB(255, 220, 220, 220),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Text(
                        "Medium Priority Task",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Center(
                        child: Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("No Task Available"),
                    ))
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
