import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sambhav/homepage.dart';

class SplashServices {
  void isLogin(BuildContext context) {
    Timer(
        Duration(seconds: 8),
        () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomePage())));
  }
}
